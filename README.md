# Reproducing Botwina (2017)

This repo exists to reproduce the study described in
[Botwina 2017](https://academicworks.cuny.edu/hc_sas_etds/154/)

# Outline of Methods Section

## Synopsis

Participants were put in one of 3 condition groups:

1. Control: did not receive the maze task at all.
2. Mild ego depletion: did maze task with normal directional controls.
3. Strong ego depletion: did maze task with the vertical axis mirrored- moving
   the mouse up would move the maze cursor down, and vice-versa.

The order of tasks presented were:

1. Mirrored maze ego depletion task (but not for control group): participants
   must navigate a point through a narrow channel in the shape of a star or a
   spiral. If they "fall out of the channel", they must restart the maze.
2. Flanker interference task: participants presented with congruent/incongruent
   (>> > >> = congruent, >> < >> = incongruent) or go/nogo (♦♦ < ♦♦ = go, XX <
   XX = no-go). The participant is instructed to indicate the direction pointed
   by the target arrow (shown here as the center arrow), except for the no-go
   condition, where they should not press anything.
3. Image-naming task: participants are prompted with hand-drawn pictures and
   asked to name them.
4. Michigan Test of English Proficiency (MTELP): participants listen to recorded
   statements and must answer multiple-choice questions related to them.
5. Language-history survey.

## Details (summarized from Botwina 2017)

All task performed with E-Prime software V 1.0 & 2.0

Three participant groups: control, mild ego depletion, strong ego depletion.

All tasks completed:

- Respective ego depletion task* *(Not control group)
- Three interference tasks
- Two language proficiency tasks
- survey

Test order: Ego, Flanker, go/no-go, image naming, MTELP, survey

## Language tasks
### Image-naming task (Peabody) [8 practice, 36 experimental]

- 260 pictures with consensus'd name
- Participants asked to name black/white variant of photo
- No feedback at all
- Picture in center of screen
- Pictures selected so "their names represented a wide range of word frequencies."
- Textbox under the image
- No time constraint
- Press enter, "After which the image appeared on the screen" Fig. 4
- Only accuracy scored (time tracked, however)
  - Critera was only the first word typed, if it matched the consensus name.
  - If no match, two english-speaking raters chose what the first full word was and if it was correct. (match or no match) [Implied indendant judgement - if disagreement then conference]
  - Though, extraordinary misspellings were correct

### Michigan Test of English Profciency (MTELP)

		Used headphones.
		2 practice
		Heard statements (could not be repeated), then asked questions related to the statement
		Multiple choice of 3 used with keyboard
		Unlimited time
		Each question one point, maximum score: 45

## Ego Depletion task
### Mirrored Maze task

		Instcted to move curser from start box to end box stay in path
			Start and end boxes "on top" (Fig. 3)
		Click mouse to start task
		Shape: Star with smaller star inside
			Path created between the two
			Outline in back
		If path moved out of, path disapear, start over
		If approached end box from right side, start over
		Difference accross groups
			Mild ego: axes normal
			Strong ego: up & down axis (*only*) was reversed
		When participants completed task, new star path appeared, smaller
		Task lasts, no matter what, 5 minutes
		Intructions to call the experimentor

## Interference tasks:

### Flanker
		Pay attention to one arrow and ignore all other distractors
			Could be on either side of the central target
			Main arrow: Red
			4 Distractor arrows: black
			Varient positions: 1,2,3 in Fig. 1
		devided into Four "blocks of items"
			Control (2 blocks) [6 practice, 12 experimental trials per block]]
				Center, red arrow facing left or right
				Participants instructed to press coresponding mouse button
			Congruent/Incongruent (2 blocks) [Also refered to as conflicted trials] [12 practice, 36 experimental trials per block]
				Black arrows that could point in either same or opposing direction were distractors.
				Red arrow was in center or "one position right or left of center"
			Go/no-go (2 blocks) [12 practice, 36 experimental per block]
				Red arrow surrounded by array of diamonds or black Xs
				"When surrounded by diamonds participants had to press the corresponding button to stimuli" [Implies this is different, somehow?]
			Mixed (1 block) [12 practice, 72 experimental per block]
				Congruent, incongurnt, go, no-go trials.
				Randomly ordered
				"The participantsparticipants had to inhibit their responses on 25 % of the trials and respond to 75% of the trials (neutral go trials and both conflict trial types" [What]
		Two potential orders of tasks:
			1) Control, 2) Go/No-Go, 3) Conflict, 4) Mixed, 5) Conflict, 6) Go/No-Go, 7) control
			 1) Control, 2) Conflict, 3) Go/No-Go, 4) Mixed, 5) Go/No-Go, 6) Conflict, and 7) Control
		Participants were asked if they needed more practice trials\
		Every trial started with a fixation cross of 250ms
		Stimuli for 2000ms or until responded
		Correct feedback was smiley emoji 750ms
		Negitive feedback was angry emoji 750ms
		All trials ended with blank screen 250ms

## Survey Questions:

	Rated their procifiency in native and max two other languages (Speaking, reading, hearing)
	1-5 scale
	Reported age at which languages learned
	Asked about teo periods of life-childhood and present
	Asked when moved to english-speaking country
	Asked gdner, age, handedness
	Asked frequency and proficiency of computers and programing languages
	Participants categorized into monolingual, LLBB, or trilingual
	
## Notes on actual experiment:
	
	Having practice problems every block feels really condecending - flanker task
	Idk if it was a hardware error bug or intended but the only key that I could click to go past the audio trials was "0"
	I didn't do the ego deletion task because it wasn't on that computer (or something?)
	survey was done on paper after the test if it matters

	
