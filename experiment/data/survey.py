import json
from itertools import groupby
from cytoolz import curried as z

data = json.load(open('survey.js'))

def jsp_likert(adict):
    options = adict['options']
    questions = adict['questions']
    return {
        'type': 'survey-likert',
        'preamble': adict.get('text'),
        'questions': [
            {'prompt': text, 'labels': options}
            for text in questions
        ]
    }


def jsp_text(adict):
    text = adict.get('text')
    return {
        'type': 'survey-text',
        'questions': [
            {'prompt': text}
        ]
    }


def jsp_multi(adict):
    text = adict.get('text')
    return {
        'type': 'survey-multi-choice',
        'questions': [
            {
                'prompt': text,
                'options': adict.get('options')
            }
        ],
    }


def jsp_instructions(adict):
    text = adict.get('instructions')
    if text is None or len(text) == 0:
        return None
    return {
        'type': 'instructions',
        'key_forward': 'space',
        'pages': [text]
    }


def dict_to_seq(adict):
    if isinstance(adict, list):
        for item in adict:
            yield from dict_to_seq(item)
    elif isinstance(adict, str):
        yield ('likert', adict)
    else:
        if 'instructions' in adict:
            yield jsp_instructions(adict)
        if adict.get('type') == 'free-text':
            yield jsp_text(adict)
        if adict.get('type') in ['single-choice', 'multi-choice']:
            yield jsp_multi(adict)
        if adict.get('type') == 'radio-row':
            yield jsp_likert(adict)
        elif 'questions' in adict:
            for question in adict['questions']:
                yield from dict_to_seq(question)


def combine_trials(trials):
    head = trials[0]
    rest = trials[1:]
    for trial in rest:
        head['questions'].extend(trial['questions'])
    if head.get('questions'):
        head['data'] = {'questions': head['questions']}
    else:
        head['data'] = {'questions': head['pages']}
    return head


trials = filter(None, dict_to_seq(data))
trials = [combine_trials(list(group))
          for _, group in groupby(trials, z.get('type'))]

trials = json.dumps(list(trials))

print("""
// this code was generated by the python script in data/survey.py

window.surveyTrials = {{trials: {trials}}};

""".format(trials=trials))
