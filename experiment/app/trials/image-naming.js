/*

This file defines the peabody image-naming tasks. It adds a variable to the
global namespace:

    window.imageNamingTrials

which contains a `trials` key, which contain the jsPsych trials for the entire
peabody section of the experiment.

The task is created using the jsPsych survey-text plugin.

TODO:
- add section name (peabody-practice, peabody-experimental)
- disable button and allow pressing enter to progress trial.
- can we force a response?

*/

(function(){
    var practice = [
	{ "filename": "ball.jpg" },
	{ "filename": "bathtub.jpg" },
	{ "filename": "caterpillar.jpg" },
	{ "filename": "flower.jpg" },
	{ "filename": "glass.jpg" },
	{ "filename": "hair.jpg" },
	{ "filename": "queen.jpg" },
	{ "filename": "sock.jpg" },
	{ "filename": "train.jpg" }
    ];

    var experimental = [
	{ "category": "animal", "filename": "squirrel.jpg" },
	{ "category": "tool", "filename": "lamp.jpg" },
	{ "category": "misc", "filename": "heart.jpg" },
	{ "category": "person", "filename": "clown.jpg" },
	{ "category": "animal", "filename": "raccoon.jpg" },
	{ "category": "insect", "filename": "fly.jpg" },
	{ "category": "tool", "filename": "comb.jpg" },
	{ "category": "nature", "filename": "mountain.jpg" },
	{ "category": "animal", "filename": "kangaroo.jpg" },
	{ "category": "vehicle", "filename": "rocket.jpg" },
	{ "category": "nature", "filename": "moon.jpg" },
	{ "category": "clothing", "filename": "necklace.jpg" },
	{ "category": "building", "filename": "castle.jpg" },
	{ "category": "tool", "filename": "spoon.jpg" },
	{ "category": "vehicle", "filename": "airplane.jpg" },
	{ "category": "furniture", "filename": "clock.jpg" },
	{ "category": "instrument", "filename": "guitar.jpg" },
	{ "category": "toy", "filename": "robot.jpg" },
	{ "category": "fruit\/veg", "filename": "carrot.jpg" },
	{ "category": "body", "filename": "beard.jpg" },
	{ "category": "tool", "filename": "skis.jpg" },
	{ "category": "instrument", "filename": "harp.jpg" },
	{ "category": "animal", "filename": "crab.jpg" },
	{ "category": "clothing", "filename": "glasses.jpg" },
	{ "category": "furniture", "filename": "vase.jpg" },
	{ "category": "animal", "filename": "horse.jpg" },
	{ "category": "instrument", "filename": "violin.jpg" },
	{ "category": "tool", "filename": "hammer.jpg" },
	{ "category": "insect", "filename": "spider.jpg" },
	{ "category": "clothing", "filename": "helmet.jpg" },
	{ "category": "fruit\/veg", "filename": "asparagus.jpg" },
	{ "category": "animal", "filename": "rabbit.jpg" },
	{ "category": "insect", "filename": "snail.jpg" },
	{ "category": "tool", "filename": "kettle.jpg" },
	{ "category": "clothing", "filename": "crown.jpg" },
	{ "category": "person", "filename": "soldier.jpg" }
    ];

    // path to the image directory
    var imagePrefix = "img/picture-naming/";

    // takes a single object from one of the two lists above and returns a
    // jsPsych survey-text trial. The original data is stored in the `stimulus`
    // key.
    function createTrial(item){
	return {
            type: 'survey-text',
            data: {
                stimulus: item,
            },
            questions: [{prompt: `<img src="${imagePrefix + item.filename}"/>`}]
	}
    }

    var allTrials = practice.map(createTrial)
        .concat(experimental.map(createTrial));

    window.imageNamingTrials = {
	trials: allTrials
    };

})();
