/*

This file defines the flanker task. It adds the following function to the
global namespace:

    window.flankerTrials()

When passed either "go" or "conflict" will result in either of the two full flanker experiments to be run.
If passed anything else will deploy the developer version.

This experiment uses the following data coding:
	flanker_order: Records which distict order of the flanker task the user did. ["go/no-go first"/"conflict first"]
	target_direction: Which way the red, target arrow is facing. ["right"/"left"]
	stim_type: Which kind of stimuli was displayed. ["control"/"go/no-go"/"conflict"]
	target_position: Which position the red, target arrow was among the distractors, control stimulus is always center. ["left"/"right"/"center"]
	stim_variation: Which variation of the stim_type was displayed. ["go"/"no-go"/"congruent"/"incongruent"/"control"]
	coded_response: Which direction the participant pressed. ["left"/"right"]
	accuracy: Whether or not the participant was correct. [true/false]
	experimental: Whether or not the trial was in experimental phase or practice phase. [true/false]
	block: Which block of the flanker task was the trial within. ["mixed"/"control"/"go"/"conflict"]

The task is created using the jsPsych survey-text plugin.

To Do:
---
Embed the original emoji pictures in feedback screens
*/

var make_grand_array = function(array, reps) {
	grand_array = [];
	var a;
	for (a = 0; a < reps; a++) {
		grand_array = grand_array.concat(array);
	};
	return grand_array
};

var fixation_cross = {
	type: "html-keyboard-response",
	stimulus: "<font size='75'>+</font>",
    choices: jsPsych.NO_KEYS,
    trial_duration: 250,
	data: {timestamp: Date.now()}
};

var feedback_screen = {
    type: "html-keyboard-response",

    stimulus: function(data) {
        //accuracy_result = "<font size='75'>CORRECT</font>";
        //console.log(jsPsych.data.get().last(1).values()[0].accuracy);
        if (jsPsych.data.get().last(1).values()[0].accuracy== true) {
            return "<font size='75' color='green'>☺</font>"
            }
        else {
             return "<font size='75' color='red'>☹</font>"
            }
    },

    choices: jsPsych.NO_KEYS,
    trial_duration: 750,
    data: {timestamp: Date.now()},
	post_trial_gap: 250,

    }
	
var make_trial = function(block, practice = false) {
	// block will be recorded as a string in data
	if (!practice) {
		post_trial_gap = 250
	} else {
		post_trial_gap = null
	}
	
	return {
		type : "html-keyboard-response",
		stimulus: jsPsych.timelineVariable("stimulus"),
		data: jsPsych.timelineVariable("data"),
		choices : [37,39],
		post_trial_gap: post_trial_gap,
		stimulus_duration: 2000,
		trial_duration: 2000,
		response_ends_trial: true,
		on_finish: function(data) {

			data.raw_response = data.key_press;

			data.section = {name: "flanker"}

			data.timestamp = Date.now()

			if (data.key_press == 37) { var input = "left"} else if (data.key_press == 39) {var input = "right"} else {var input = "none"};
			data.coded_response = input;

			if (data.stim_variation == "no-go") {
				if (input == "none") {data.accuracy = true} else {data.accuracy = false}
			}
			else if (input == data.target_direction) {data.accuracy = true}
			else {data.accuracy = false}
			
			if (practice) {
				data.experimental = false
			} else {
				data.experimental = true
			}
			
			data.block = block // to record what block the trial was in
		}
	}
}


var control_stimulus = [
	{stimulus : "<font size='75' color='red'><</font>",
	data: {target_direction: "left", stim_type: "control", target_position: "center", stim_variation: "control"}},

	{stimulus : "<font size='75' color='red'>></font>",
	data: {target_direction: "right", stim_type: "control", target_position: "center", stim_variation: "control"}}
];

var go_stimulus = [
	{stimulus: "<font size='75'>XX<font color='red'>></font>XX</font>",
	data: {target_direction: "right", stim_type: "go/no-go", target_position: "center", stim_variation: "no-go"}},

	{stimulus: "<font size='75'>XX<font color='red'><</font>XX</font>",
	data: {target_direction: "left", stim_type: "go/no-go", target_position: "center", stim_variation: "no-go"}},

	{stimulus: "<font size='75'>X<font color='red'>></font>XXX</font>",
	data: {target_direction: "right", stim_type: "go/no-go", target_position: "left", stim_variation: "no-go"}},

	{stimulus: "<font size='75'>X<font color='red'><</font>XXX</font>",
	data: {target_direction: "left", stim_type: "go/no-go", target_position: "left", stim_variation: "no-go"}},

	{stimulus: "<font size='75'>XXX<font color='red'>></font>X</font>",
	data: {target_direction: "right", stim_type: "go/no-go", target_position: "right", stim_variation: "no-go"}},

	{stimulus: "<font size='75'>XXX<font color='red'><</font>X</font>",
	data: {target_direction: "left", stim_type: "go/no-go", target_position: "right", stim_variation: "no-go"}},

	{stimulus: "<font size='75'>◇◇<font color='red'>></font>◇◇</font>",
	data: {target_direction: "right", stim_type: "go/no-go", target_position: "center", stim_variation: "go"}},

	{stimulus: "<font size='75'>◇◇<font color='red'><</font>◇◇</font>",
	data: {target_direction: "left", stim_type: "go/no-go", target_position: "center", stim_variation: "go"}},

	{stimulus: "<font size='75'>◇<font color='red'>></font>◇◇◇</font>",
	data: {target_direction: "right", stim_type: "go/no-go", target_position: "left", stim_variation: "go"}},

	{stimulus: "<font size='75'>◇<font color='red'><</font>◇◇◇</font>",
	data: {target_direction: "left", stim_type: "go/no-go", target_position: "left", stim_variation: "go"}},

	{stimulus: "<font size='75'>◇◇◇<font color='red'>></font>◇</font>",
	data: {target_direction: "right", stim_type: "go/no-go", target_position: "right", stim_variation: "go"}},

	{stimulus: "<font size='75'>◇◇◇<font color='red'><</font>◇</font>",
	data: {target_direction: "left", stim_type: "go/no-go", target_position: "right", stim_variation: "go"}}
];

var conflict_stimulus = [
	{stimulus: "<font size='75'>>><font color='red'>></font>>></font>",
	data: {target_direction: "right", stim_type: "conflict", target_position: "center", stim_variation: "congruent"}},

	{stimulus: "<font size='75'>>><font color='red'><</font>>></font>",
	data: {target_direction: "left", stim_type: "conflict", target_position: "center", stim_variation: "incongruent"}},

	{stimulus: "<font size='75'>><font color='red'>></font>>>></font>",
	data: {target_direction: "right", stim_type: "conflict", target_position: "left", stim_variation: "congruent"}},

	{stimulus: "<font size='75'>><font color='red'><</font>>>></font>",
	data: {target_direction: "left", stim_type: "conflict", target_position: "left", stim_variation: "incongruent"}},

	{stimulus: "<font size='75'>>>><font color='red'>></font>></font>",
	data: {target_direction: "right", stim_type: "conflict", target_position: "right", stim_variation: "congruent"}},

	{stimulus: "<font size='75'>>>><font color='red'><</font>></font>",
	data: {target_direction: "left", stim_type: "conflict", target_position: "right", stim_variation: "incongruent"}},

	{stimulus: "<font size='75'><<<font color='red'>></font><<</font>",
	data: {target_direction: "right", stim_type: "conflict", target_position: "center", stim_variation: "incongruent"}},

	{stimulus: "<font size='75'><<<font color='red'><</font><<</font>",
	data: {target_direction: "left", stim_type: "conflict", target_position: "center", stim_variation: "congruent"}},

	{stimulus: "<font size='75'><<font color='red'>></font><<<</font>",
	data: {target_direction: "right", stim_type: "conflict", target_position: "left", stim_variation: "incongruent"}},

	{stimulus: "<font size='75'><<font color='red'><</font><<<</font>",
	data: {target_direction: "left", stim_type: "conflict", target_position: "left", stim_variation: "congruent"}},

	{stimulus: "<font size='75'><<<<font color='red'>></font><</font>",
	data: {target_direction: "right", stim_type: "conflict", target_position: "right", stim_variation: "incongruent"}},

	{stimulus: "<font size='75'><<<<font color='red'><</font><</font>",
	data: {target_direction: "left", stim_type: "conflict", target_position: "right", stim_variation: "congruent"}}
]

var mixed_stimulus = function() {
	// Takes three stimulus form each of the four stim variations in conflict and go/no-go blocks
	out_stimulus = []
	
	possible_stimulus = [[{stimulus: "<font size='75'>XX<font color='red'>></font>XX</font>",
	data: {target_direction: "right", stim_type: "go/no-go", target_position: "center", stim_variation: "no-go"}},

	{stimulus: "<font size='75'>XX<font color='red'><</font>XX</font>",
	data: {target_direction: "left", stim_type: "go/no-go", target_position: "center", stim_variation: "no-go"}},

	{stimulus: "<font size='75'>X<font color='red'>></font>XXX</font>",
	data: {target_direction: "right", stim_type: "go/no-go", target_position: "left", stim_variation: "no-go"}},

	{stimulus: "<font size='75'>X<font color='red'><</font>XXX</font>",
	data: {target_direction: "left", stim_type: "go/no-go", target_position: "left", stim_variation: "no-go"}},

	{stimulus: "<font size='75'>XXX<font color='red'>></font>X</font>",
	data: {target_direction: "right", stim_type: "go/no-go", target_position: "right", stim_variation: "no-go"}},

	{stimulus: "<font size='75'>XXX<font color='red'><</font>X</font>",
	data: {target_direction: "left", stim_type: "go/no-go", target_position: "right", stim_variation: "no-go"}}],
	
	
	[{stimulus: "<font size='75'>◇◇<font color='red'>></font>◇◇</font>",
	data: {target_direction: "right", stim_type: "go/no-go", target_position: "center", stim_variation: "go"}},

	{stimulus: "<font size='75'>◇◇<font color='red'><</font>◇◇</font>",
	data: {target_direction: "left", stim_type: "go/no-go", target_position: "center", stim_variation: "go"}},

	{stimulus: "<font size='75'>◇<font color='red'>></font>◇◇◇</font>",
	data: {target_direction: "right", stim_type: "go/no-go", target_position: "left", stim_variation: "go"}},

	{stimulus: "<font size='75'>◇<font color='red'><</font>◇◇◇</font>",
	data: {target_direction: "left", stim_type: "go/no-go", target_position: "left", stim_variation: "go"}},

	{stimulus: "<font size='75'>◇◇◇<font color='red'>></font>◇</font>",
	data: {target_direction: "right", stim_type: "go/no-go", target_position: "right", stim_variation: "go"}},

	{stimulus: "<font size='75'>◇◇◇<font color='red'><</font>◇</font>",
	data: {target_direction: "left", stim_type: "go/no-go", target_position: "right", stim_variation: "go"}}],
	
	
	[{stimulus: "<font size='75'>>><font color='red'>></font>>></font>",
	data: {target_direction: "right", stim_type: "conflict", target_position: "center", stim_variation: "congruent"}},

	{stimulus: "<font size='75'>><font color='red'>></font>>>></font>",
	data: {target_direction: "right", stim_type: "conflict", target_position: "left", stim_variation: "congruent"}},

	{stimulus: "<font size='75'>>>><font color='red'>></font>></font>",
	data: {target_direction: "right", stim_type: "conflict", target_position: "right", stim_variation: "congruent"}},

	{stimulus: "<font size='75'><<<font color='red'><</font><<</font>",
	data: {target_direction: "left", stim_type: "conflict", target_position: "center", stim_variation: "congruent"}},

	{stimulus: "<font size='75'><<font color='red'><</font><<<</font>",
	data: {target_direction: "left", stim_type: "conflict", target_position: "left", stim_variation: "congruent"}},

	{stimulus: "<font size='75'><<<<font color='red'><</font><</font>",
	data: {target_direction: "left", stim_type: "conflict", target_position: "right", stim_variation: "congruent"}}],
	
	
	[{stimulus: "<font size='75'>>><font color='red'><</font>>></font>",
	data: {target_direction: "left", stim_type: "conflict", target_position: "center", stim_variation: "incongruent"}},

	{stimulus: "<font size='75'>><font color='red'><</font>>>></font>",
	data: {target_direction: "left", stim_type: "conflict", target_position: "left", stim_variation: "incongruent"}},

	{stimulus: "<font size='75'>>>><font color='red'><</font>></font>",
	data: {target_direction: "left", stim_type: "conflict", target_position: "right", stim_variation: "incongruent"}},

	{stimulus: "<font size='75'><<<font color='red'>></font><<</font>",
	data: {target_direction: "right", stim_type: "conflict", target_position: "center", stim_variation: "incongruent"}},

	{stimulus: "<font size='75'><<font color='red'>></font><<<</font>",
	data: {target_direction: "right", stim_type: "conflict", target_position: "left", stim_variation: "incongruent"}},

	{stimulus: "<font size='75'><<<<font color='red'>></font><</font>",
	data: {target_direction: "right", stim_type: "conflict", target_position: "right", stim_variation: "incongruent"}}]]
	
	var stim;
	for (var i = 0;i < possible_stimulus.length;i++){
		stim = jsPsych.randomization.repeat(possible_stimulus[i], 1, false) //shuffles the original stimulus
		for (var a = 0;a < 3;a++) {
			out_stimulus = out_stimulus.concat(stim[a])
		}
	}
	return out_stimulus
}

mixed_stimulus()

var control_instructions = {
	type: "instructions",
	pages: [`<font size='5'><p>In this task, you will see arrows in the middle of the screen.</p>
	<p>Your task is to press the arrow key that corresponds to the direction of the arrow.</p>
	<p>Always press SPACE to move to the next screen.</p></font>`,
	
	`<font size='5'><p>For instance, if the arrow looks like this:</p>
	<font size='10' color=red><p> < </p></font>
	<p>You would press the LEFT arrow.</p></font>`,
	
	`<font size='5'><p>If the arrow looks like this:</p>
	<font size='10' color=red><p> > </p></font>
	<p>You would press the RIGHT arrow.</p></font>`,
	
	`<font size='5'><p>You will first do some practice trials.</p>
	<p>In the practice session, you will receive feedback on your responses.</p>
	<p>Remember to go AS FAST AS YOU CAN without making mistakes.</p>
	<p>Press SPACE when you are ready to begin.</p></font>`],

    key_forward: 'space',
	allow_backward: false
};

var go_instructions = {
	type: "instructions",
	pages: [`<font size='5'><p>In this task, you will see a RED arrow.</p>
            <p>They can be surrounded by X's or ◇'s.</p>
			<p>Always press SPACE to continue.</p></font>`,
			
            `<font size='5'><p>For instance, if you see a line of line of shapes like this:</p>
            <p><font size='10'>◇<span style="color:red"><</span>◇◇◇</p></font>
            <p>You would press the LEFT arrow.</p></font>`,
			
			`<font size='5'><p>If you see a line of line of shapes like this:</p>
            <font size='10'><p>◇◇◇<span style="color:red">></span>◇</p></font>
            <p>You would press the RIGHT arrow.</p></font>`,
			
            `<font size='5'><p>HOWEVER, don't press anything if you see a line similar to this:</p>
            <font size='10'>XXX<span style="color:red"><</span>X</p></font>
			<p>or</p>
			<p><font size='10'>X<span style="color:red">></span>XXX</p></font></font>`,
			
            `<font size='5'><p>You will first do some practice trials.</p>
            <p>In the practice session, you will receive feedback on your responses.</p>
            <p>Remember to go AS FAST AS YOU CAN without making mistakes.</p>
            <p>Press SPACE when you are ready to begin.</p></font>`],

        key_forward: 'space',
		allow_backward: false

};

var conflict_instructions = {
	type: "instructions",
	pages: [`<font size='5'><p>In this task, you will see some RED and BLACK arrows in the center of the screen.</p>
	       <p>Your task is to press the arrow key that corresponds to the direction of the RED arrow.</p>
	       <p>Always press SPACE to continue.</p></font>`,
		   
	       `<font size='5'><p>For instance, if you see a line of arrows like this:</p>
	       <font size='10'><p>>>><span style="color:red"><</span>></p></font>
	       <p>You would press the LEFT arrow.</p></font>`,
		   
	       `<font size='5'><p>If you see a line of arrows like this:</p>
	       <font size='10'><p><<span style="color:red">></span><<<</p></font>
	       <p>You would press the RIGHT arrow.<p></font>`,
		   
	       `<font size='5'><p>You will first do some practice trials.</p>
           	<p>In the practice session, you will receive feedback on your responses.</p>
           	<p>Remember to go AS FAST AS YOU CAN without making mistakes.</p>
           	<p>Press the spacebar when you are ready to begin.</p></font>`],
        key_forward: 'space',
		allow_backward: false

};

var mixed_instructions = {
	type: "instructions",
	pages: [`<font size='5'><p>In the following task, you will see a mixture of items you've seen before.</p>
	        <p>Let's review these items before proceeding.</p>
            <p>Always press SPACE to continue.</p></font>`,
			
            `<font size='5'><p>If you see an item similar to one of these below, look for the RED arrow, and respond to its direction.</p>
            <p><font size='10'><<span style="color:red">></span><<<</p>
			<p>>><span style="color:red"><</span>>></p></font></font>`,
			
            `<font size='5'><p>If you see an arrow surrounded by diamonds, respond to the direction of the arrow.</p>
			<p><font size='10'>◇<span style='color:red'><</span>◇◇◇</font></p>
			<p>If you see an arrow surrounded by X's DON'T PRESS ANY BUTTONS!</p>
			<p><font size='10'>XXX<span style='color:red'><</span>X</p></font></font>`,
			
            `<font size='5'><p>You will first do some practice trials.</p>
            <p>In the practice session, you will receive feedback on your responses.</p>
            <p>Remember to go AS FAST AS YOU CAN without making mistakes.</p>
            <p>Press the spacebar when you are ready to begin.</p></font>`],

        key_forward: 'space',
		allow_backward: false
};

var end_block_screen = {
	type: "instructions",
	pages:[`<font size='5'><p>Very well done!</p>
		<p>When you are ready to begin the next section please press SPACE.</font>`],
		key_forward: 'space',
		allow_backward: false
}

var control_trials_practice = {
	timeline: [fixation_cross, make_trial("control",practice = true), feedback_screen],
	timeline_variables: make_grand_array(control_stimulus,3),
	randomize_order: true,
	on_finish: function(data) {
	data.experimental = false
	}
};

var go_trials_practice = {
	timeline: [fixation_cross, make_trial("go",practice = true), feedback_screen],
	timeline_variables: make_grand_array(go_stimulus,1),
	randomize_order: true,
	on_finish: function(data) {
	data.experimental = false
	}
};

var conflict_trials_practice = {
	timeline: [fixation_cross, make_trial("conflict",practice = true), feedback_screen],
	timeline_variables: make_grand_array(conflict_stimulus,1),
	randomize_order: true,
	on_finish: function(data) {
	data.experimental = false
	}
};

var mixed_trials_practice = {
	timeline: [fixation_cross, make_trial("mixed",practice = true), feedback_screen],
	timeline_variables: mixed_stimulus(),
	randomize_order: true,
	on_finish: function(data) {
	data.experimental = false
	}};

var control_end_practice = {
	timeline: [{
		type: "html-keyboard-response",
		stimulus: `<p><font size='5'>You have finished practice, do you want more?</p>
		<p>Yes = Y</p>
		<p>No = N</p></font>`,
		choices: ["y","n"],
		response_ends_trial: true},
		{timeline: [control_instructions, control_trials_practice],
		conditional_function: function(){
			var response = jsPsych.data.get().last(1).values()[0];
			if(response.key_press == jsPsych.pluginAPI.convertKeyCharacterToKeyCode("y")){ return true; } else { return false; }}
		},
		{type: "instructions",
		pages: [`<font size='5'><p>Good job!</p>
		<p>You will now begin the experimental trials.</p>
		<p>In the experimental session you will not recieve feedback on your responses.</p>
		<p>Remember to go AS FAST AS YOU CAN without making mistakes.</p>
		<p>Press SPACE when you are ready to begin.</p></font>`]}
	],
	key_forward: 'space',
	allow_backward: false
};

var go_end_practice = {
	timeline: [{
		type: "html-keyboard-response",
		stimulus: `<p><font size='5'>You have finished practice, do you want more?</p>
		<p>Yes = Y</p>
		<p>No = N</p></font>`,
		choices: ["y","n"],
		response_ends_trial: true},
		{timeline: [go_instructions, go_trials_practice],
		conditional_function: function(){
			var response = jsPsych.data.get().last(1).values()[0];
			if(response.key_press == jsPsych.pluginAPI.convertKeyCharacterToKeyCode("y")){ return true; } else { return false; }}
		},
		{type: "instructions",
		pages: [`<font size='5'><p>Good job!</p>
		<p>You will now begin the experimental trials.</p>
		<p>In the experimental session you will not recieve feedback on your responses.</p>
		<p>Remember to go AS FAST AS YOU CAN without making mistakes.</p>
		<p>Press SPACE when you are ready to begin.</p></font>`]}
	],
	key_forward: 'space',
	allow_backward: false
};

var conflict_end_practice = {
	timeline: [{
		type: "html-keyboard-response",
		stimulus: `<p><font size='5'>You have finished practice, do you want more?</p>
		<p>Yes = Y</p>
		<p>No = N</p></font>`,
		choices: ["y","n"],
		response_ends_trial: true},
		{timeline: [conflict_instructions, conflict_trials_practice],
		conditional_function: function(){
			var response = jsPsych.data.get().last(1).values()[0];
			if(response.key_press == jsPsych.pluginAPI.convertKeyCharacterToKeyCode("y")){ return true; } else { return false; }}
		},
		{type: "instructions",
		pages: [`<font size='5'><p>Good job!</p>
		<p>You will now begin the experimental trials.</p>
		<p>In the experimental session you will not recieve feedback on your responses.</p>
		<p>Remember to go AS FAST AS YOU CAN without making mistakes.</p>
		<p>Press SPACE when you are ready to begin.</p></font>`]}
	],
	key_forward: 'space',
	allow_backward: false
};

var mixed_end_practice = {
	timeline: [{
		type: "html-keyboard-response",
		stimulus: `<p><font size='5'>You have finished practice, do you want more?</p>
		<p>Yes = Y</p>
		<p>No = N</p></font>`,
		choices: ["y","n"],
		response_ends_trial: true},

		{timeline: [mixed_instructions, mixed_trials_practice],
		conditional_function: function(){
			var response = jsPsych.data.get().last(1).values()[0];
			if(response.key_press == jsPsych.pluginAPI.convertKeyCharacterToKeyCode("y")){ return true; } else { return false; }}
		},

		{type: "instructions",
		pages: [`<font size='5'><p>Good job!</p>
		<p>You will now begin the experimental trials.</p>
		<p>In the experimental session you will not recieve feedback on your responses.</p>
		<p>Remember to go AS FAST AS YOU CAN without making mistakes.</p>
		<p>Press SPACE when you are ready to begin.</p></font>`]}
	],
	key_forward: 'space',
	allow_backward: false
};

var control_trials_exp = {
	timeline: [fixation_cross, make_trial("control")],
	timeline_variables: make_grand_array(control_stimulus,6),
	randomize_order: true, ///timeline_variables needs to be an array of the repeated stimulus or else randomize would only sub-randomize
	on_finish: function(data) {
	data.experimental = true
	}
};

var go_trials_exp = {
	timeline: [fixation_cross, make_trial("go")],
	timeline_variables: make_grand_array(go_stimulus,3),
	randomize_order: true,
	on_finish: function(data) {
	data.experimental = true
	}};

var conflict_trials_exp = {
	timeline: [fixation_cross, make_trial("conflict")],
	timeline_variables: make_grand_array(conflict_stimulus,3),
	randomize_order: true,
	on_finish: function(data) {
	data.experimental = true
	}
};

var mixed_trials_exp = {
	timeline: [fixation_cross, make_trial("mixed")],
	timeline_variables: make_grand_array(go_stimulus,3).concat(make_grand_array(conflict_stimulus,3)),
	randomize_order: true,
	on_finish: function(data) {
	data.experimental = true
	}
};

var control_block = {timeline : [control_instructions, control_trials_practice, control_end_practice, control_trials_exp, end_block_screen]}

var control_block_last = {timeline : [control_instructions, control_trials_practice, control_end_practice, control_trials_exp]}

var go_block = {timeline: [go_instructions, go_trials_practice, go_end_practice, go_trials_exp, end_block_screen]}

var conflict_block = {timeline: [conflict_instructions, conflict_trials_practice, conflict_end_practice, conflict_trials_exp, end_block_screen]}

var mixed_block = {timeline: [mixed_instructions, mixed_trials_practice, mixed_end_practice, mixed_trials_exp, end_block_screen]}

var flanker_order1 = {timeline: [control_block,go_block,conflict_block,mixed_block,conflict_block,go_block,control_block_last]} /// Note: you could rename orders to, "go_order" and "conflict_order" if it was desired.

var flanker_order2 = {timeline: [control_block,conflict_block,go_block,mixed_block,go_block,conflict_block,control_block_last]}

var flanker = function(order) {

	if (order == "go") {
		jsPsych.data.addProperties({flanker_order: "go/no-go first"}); ///Note: (I think) this code adds this data to all lines of data in the experiment, not just flanker data
		return flanker_order1;
	} else if (order == "conflict") {
		jsPsych.data.addProperties({flanker_order: "conflict first"});
		return flanker_order2;
	} else { ///Here for testing reasons, do not use for experimental trials
		jsPsych.data.addProperties({flanker_order: "development-only order"})
		return mixed_block
	} /// I feel like else here should raise an error in actual use
};

window.flankerTrials = flanker