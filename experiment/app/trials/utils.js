function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function getStoredJson(key) {
    return JSON.parse(localStorage.getItem(key)) || {};
}

function storeJson(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
}

function getQueryParams() {
    let qString = window.location.search,
        queryRe = /[?&]([^=]+?)=([^?]+?)(?=&|$)/g,
        triple = null,
        params = {};

    while (triple = queryRe.exec(qString)) {
        let [_, key, val] = triple;
        params[key] = val;
    }

    return params;
}


function httpGet(url) {
    var request = new XMLHttpRequest();
    request.open('GET', url, true);

    return new Promise((resolve, reject) => {
        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                // Success!
                resolve(request.responseText);
            } else {
                // We reached our target server, but it returned an error
                reject(request);
            }
        };

        request.onerror = function() {
            reject(request);
        };

        request.send();
    });
}

function getUserMetadata() {
    let location = 'metadata',
        queryParams = getQueryParams(),
        storedParams = getStoredJson(location),

        // mturk data
        workerId = queryParams['workerId'] || '',
        assignmentId = queryParams['assignmentId'] || '',
        hitId = queryParams['hitId'] || '',

        metadata = {
            participantId: workerId || queryParams['id'] || uuid(),
            browserId: storedParams['browserId'] || uuid(),
            sessionId: window['sessionId'] || uuid(),
            workerId: workerId,
            assignmentId: assignmentId,
            hitId: hitId,
            ip: httpGet('https://api.ipify.org')
        };

    window['sessionId'] = metadata['sessionId'];

    storeJson(location, metadata);

    return metadata;
}
