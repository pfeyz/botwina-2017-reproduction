var metadata = getUserMetadata();

var globalData = {
    session_id: metadata['sessionId'],
    browser_id: metadata['browserId'],
    depletion_condition: 'mild',
}

jsPsych.data.addProperties(globalData);

// add ip address to metadata once it's available.
metadata.ip.then(ip => {
    globalData['browserIp'] = ip;
    console.log(globalData);
});

var timelineSections = [
    window.flankerTrials("go"), //Can specifiy "go" or "conflict" for the two distinct orders. Anything else will start the developer order
    window.surveyTrials.trials,
    //window.imageNamingTrials.trials,
];

var timeline = timelineSections.reduce((acc, item) => acc.concat(item), []);

function recordData(data) {
    let database = window['firebase'].database().ref(`session/${data.session_id}`);
    if (!database) {
        throw new Error();
    }
    console.log(data);
    database.push(data);
}

jsPsych.init({
    timeline: timeline,
    on_trial_finish: recordData,
    on_finish: function(){ jsPsych.data.displayData(); }
});
